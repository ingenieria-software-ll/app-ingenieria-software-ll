import { Component, OnInit, NgZone} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule } from '@angular/material/input';
import { Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})



export class LoginComponent implements OnInit {
 errorMessage = '';
 constructor(private afAuth: AngularFireAuth,
   private router: Router,
   private fb: FormBuilder,
   private ngZone: NgZone) { }
   
username: string;
password: string;


 ngOnInit() {
   }
   login() : void {
    this.afAuth.signInWithEmailAndPassword(this.username, this.password).then(() => {
      localStorage.setItem('showNavbar', "1" );
      this.router.navigate(["users"]);
      //window.location.reload();
    }).catch(response => {
      this.errorMessage = response.message;
      alert("Invalid credentials");
    });
  }
}