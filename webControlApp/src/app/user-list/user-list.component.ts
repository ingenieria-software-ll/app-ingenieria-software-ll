import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/user.model';
import { element } from 'protractor';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { fromEvent} from 'rxjs';
import { AddComponent } from "../dialogs/add/add.component";
import { EditComponent } from "../dialogs/edit/edit.component";
import { DeleteComponent } from "../dialogs/delete/delete.component";


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})

export class UserListComponent implements OnInit {
  
  displayedColumns = ['Name', 'Lastname', 'Identification', 'Phone', 'Email', 'actions'];
  users: User[] = [];
  usersFiltered: User[] = [];
  id: string;
  identificationUser: string;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;


  constructor(public dialog: MatDialog,
    public userService: UserService,) { }

  ngOnInit(): void {
    
    this.loadData();
    fromEvent(this.filter.nativeElement, 'keyup')
      .subscribe(() => {
        if (this.filter.nativeElement.value === '') {
          this.usersFiltered = this.users;
        } else {
          this.usersFiltered = this.users.filter(user => user.identificationUser !== undefined && user.identificationUser !== null && user.identificationUser.indexOf(this.filter.nativeElement.value) !== -1);
        }
      })
  }

  refresh(): void {
    this.loadData();
  }



  public async loadData() {
    this.userService.getUser().subscribe(data => {
      this.users = data.map(e => {
        let user = e.payload.doc.data();
        return {
          id: e.payload.doc.id,
          nameUser: user['nameUser'], 
          lastNameUser: user['lastNameUser'],
          identificationUser: user['identificationUser'],
          phoneUser: user['phoneUser'],
          emailUser: user['emailUser']

        } as User
      });;;
      this.usersFiltered = this.users;
    });
  }

  
  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  addNew() {
    const dialogRef = this.dialog.open(AddComponent, {
      data: { user: User }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refreshTable();
      }
    });
  }

  startEdit(id: string, nameUser: string, lastNameUser: string, identificationUser: string, phoneUser: string,emailUser: string) {
    this.id= id;
    
    console.log(this.id);
    debugger;
    const dialogRef = this.dialog.open(EditComponent, {
      data: { id: id, nameUser: nameUser, lastNameUser: lastNameUser,identificationUser: identificationUser, phoneUser: phoneUser,emailUser: emailUser}    
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refreshTable();
      }
    });
  }

  deleteItem(id: string, nameUser: string, lastNameUser: string,identificationUser: string, phoneUser: string,emailUser: string) {
    this.identificationUser = identificationUser;
    const dialogRef = this.dialog.open(DeleteComponent, {
      data: { id: id, nameUser: nameUser, lastNameUser: lastNameUser,identificationUser: identificationUser, phoneUser: phoneUser,emailUser: emailUser}
      
    })
    debugger;;

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refreshTable();
      }
    });
  }



}
