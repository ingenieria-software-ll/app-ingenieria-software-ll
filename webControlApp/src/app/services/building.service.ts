import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Key } from 'protractor';
import { Building } from '../model/building.model';

@Injectable({
  providedIn: 'root'
})
export class BuildingService {

  selectedBuilding: Building = new Building();
  dialogData: any;
  constructor(private firestore: AngularFirestore) { }


  getBuilding() {
    return this.firestore.collection('Edificios').snapshotChanges();
  }

  createBuilding(building: Building){
    return this.firestore.collection('Edificios').add(JSON.parse(JSON.stringify(building))); 
  }

  deleteBuilding(data: any){
    let id= data.id;
    this.firestore.doc('Edificios/' + id).delete();
  }
  updateBuildings(data : any){
    let idBuilding= data.idBuilding;
    delete data.idBuilding;
    this.firestore.collection('Edificios').doc(idBuilding).update(data);
    
  }

}
