import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from 'src/app/model/user.model';
import { Key } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  selectedUser: User = new User();
  dialogData: any;
  constructor(private firestore: AngularFirestore) { }


  getUser() {
    return this.firestore.collection('visitantes').snapshotChanges();
  }
  getUser1(identificationUser: string){
    return this.firestore.collection('visitantes').doc(identificationUser).snapshotChanges();
  }
  createUser(user: User){
    return this.firestore.collection('visitantes').add(JSON.parse(JSON.stringify(user))); 
  }


  updateUser(id: string, data:any){
    this.firestore.doc('user/' + id).update(data);
  }

  deleteUser(data: any){
    let id= data.id;
    this.firestore.doc('visitantes/' + id).delete();
  }
  updateUsers(data : any){
    let id= data.id;
    delete data.id;
    this.firestore.collection('visitantes').doc(id).update(data);
    
  }

}
