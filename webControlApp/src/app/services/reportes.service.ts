import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { DatePipe } from '@angular/common';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root'
})
export class ReportesService {

  constructor(private angularfirestore: AngularFirestore) {
    
   }

   getRegistrados(){
    
    return this.angularfirestore.collection('RegistrodeAcceso');
}

getRegistradosPorEdificio( idEdificio: string){
  return this.angularfirestore.collection('RegistrodeAcceso', ref => ref.where('edificio', ">=", idEdificio)).snapshotChanges();
}

getRegistradosPorHoraIngreso(horaIngreso: DatePipe){
  return this.angularfirestore.collection('RegistrodeAcceso', ref => ref.where('horaIngreso', '==', horaIngreso)).snapshotChanges();
}

getRegistradosPorHoraEgreso(horaEgreso: DatePipe){
  return this.angularfirestore.collection('RegistrodeAcceso', ref => ref.where('horaEgreso', '==', horaEgreso)).snapshotChanges();
}

getRegistradosPorZona(idZona: string){
  return this.angularfirestore.collection('RegistrodeAcceso', ref => ref.where('zona', '==', idZona)).snapshotChanges();
}


// ConvertToCSV(objArray, headerList) {
//   let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
//   let str = '';
//   let row = 'S.No,';
//   for (let index in headerList) {
//    row += headerList[index] + ',';
//   }
//   row = row.slice(0, -1);
//   str += row + '\r\n';
//   for (let i = 0; i < array.length; i++) {
//    let line = (i+1)+'';
//    for (let index in headerList) {
//     let head = headerList[index];
//     line += ',' + array[i][head];
//    }
//    str += line + '\r\n';
//   }
//   return str;
//  }

//  downloadFile(data, filename='Reporte') {
//   let csvData = this.ConvertToCSV(data, ['Cedula','Edificio', 'Email Registrador', 'Hora Ingreso', 'Hora Egreso', 'Temperatura', 'Zona']);
//   console.log(csvData)
//   let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
//   let dwldLink = document.createElement("a");
//   let url = URL.createObjectURL(blob);
//   let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
//   if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
//       dwldLink.setAttribute("target", "_blank");
//   }
//   dwldLink.setAttribute("href", url);
//   dwldLink.setAttribute("download", filename + ".csv");
//   dwldLink.style.visibility = "hidden";
//   document.body.appendChild(dwldLink);
//   dwldLink.click();
//   document.body.removeChild(dwldLink);
// }



}


