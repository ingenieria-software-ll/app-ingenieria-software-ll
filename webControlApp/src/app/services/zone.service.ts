import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Key } from 'protractor';
import { Zone } from '../model/zone.model';

@Injectable({
  providedIn: 'root'
})
export class ZoneService {

  selectedZone: Zone = new Zone();
  dialogData: any;
  constructor(private firestore: AngularFirestore) { }


  getZone(idBuilding: string) {
    return this.firestore.collection('Edificios').doc(idBuilding).collection('zona').snapshotChanges();
  }

  createZone(idBuilding: string, zone: Zone){
    debugger;
    return this.firestore.collection('Edificios').doc(idBuilding).collection('zona').add(JSON.parse(JSON.stringify(zone))); 
  }

  deleteZone(idBuilding: string, zone: Zone){
    debugger;
    let idZone= zone.idZone;
    this.firestore.collection('Edificios').doc(idBuilding).collection('zona').doc(idZone).delete();
  }
  updateZones(idBuilding: string, zone: Zone){
    let idZone= zone.idZone;
    delete zone.idZone;
    debugger;
    this.firestore.collection('Edificios').doc(idBuilding).collection('zona').doc(idZone).update(zone);
    
  }

}
