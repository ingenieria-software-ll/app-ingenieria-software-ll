import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from "@angular/router";
import { UserService } from './services/user.service';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { AppService } from './services/app.service';
import { ReportesService } from './services/reportes.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'webcontrolcovid';
  showNavbar:boolean = false ;

  constructor(public router: Router, private appService: AppService, private reportesServices: ReportesService){}

  
// download(){
//   this.appService.downloadFile(, 'jsontocsv');
// }


  ngOnInit(){
    this.router.events.subscribe((event: RouterEvent) => {
      if (event.url=='/login'){
        this.showNavbar = false;
      }else if(event.url!==undefined){
        this.showNavbar = true;
      }
    });
  }
  
}


