import { Component, OnInit } from '@angular/core';
import { Router, Params } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor( public router:Router) { }

  ngOnInit(): void {
  }


  logOut():void{
    localStorage.setItem('showNavbar', "0");
    this.router.navigate(["login"]);
    //window.location.reload();
  }

}
