import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import { ZoneService } from 'src/app/services/zone.service';
import { Zone } from 'src/app/model/zone.model';

@Component({
  selector: 'app-edit-zone',
  templateUrl: './edit-zone.component.html',
  styleUrls: ['./edit-zone.component.scss']
})
export class EditZoneComponent implements OnInit {

  
  constructor(public dialogRef: MatDialogRef<EditZoneComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public zoneService: ZoneService) { }

  ngOnInit(): void {
  }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
   // let zone=new Zone(undefined, this.data.idZone, this.data.zone.nameZone, this.data.zone.capacity, this.data.zone.maxCapacity);
    debugger;
    this.zoneService.updateZones(this.data.idBuilding, this.data);
    debugger;
  }

}
