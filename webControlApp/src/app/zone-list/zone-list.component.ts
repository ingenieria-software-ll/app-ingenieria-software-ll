import { ZoneService } from 'src/app/services/zone.service';
import { Zone } from 'src/app/model/zone.model';
import { element } from 'protractor';
import { Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { fromEvent} from 'rxjs';
import { AddZoneComponent } from '../dialogsZones/add-zone/add-zone.component';
import { DeleteZoneComponent } from '../dialogsZones/delete-zone/delete-zone.component';
import { EditZoneComponent } from '../dialogsZones/edit-zone/edit-zone.component';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-zone-list',
  templateUrl: './zone-list.component.html',
  styleUrls: ['./zone-list.component.css']
})
export class ZoneListComponent implements OnInit {

  displayedColumns = ['nameZone', 'capacity', 'maxCapacity', 'actions'];
  zones: Zone[] = [];
  zonesFiltered: Zone[] = [];
  idZone: string;
  idBuilding: string;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;


  constructor(public dialog: MatDialog,
    public zoneService: ZoneService, private router: ActivatedRoute) { }

  ngOnInit(): void {
    this.router.params.subscribe((params)=>{
      this.idBuilding=params['id'];
    })
    this.loadData();
    fromEvent(this.filter.nativeElement, 'keyup')
      .subscribe(() => {
        if (this.filter.nativeElement.value === '') {
          this.zonesFiltered = this.zones;
        } else {
          this.zonesFiltered = this.zones.filter(zone => zone.nameZone!== undefined && zone.nameZone !== null && zone.nameZone.indexOf(this.filter.nativeElement.value) !== -1);
        }
      })
  }

  refresh(): void {
    this.loadData();
  }



  public async loadData() {
    this.zoneService.getZone(this.idBuilding).subscribe(data => {
      this.zones = data.map(e => {
        let zone = e.payload.doc.data();
        return {
          idZone: e.payload.doc.id,
          nameZone: zone['nameZone'],
          capacity: zone['capacity'],
          maxCapacity: zone['maxCapacity']

        } as Zone
      });;;
      this.zonesFiltered = this.zones;
    });
  }

  
  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  addNew() {
    const dialogRef = this.dialog.open(AddZoneComponent, {
    data: { zone: Zone, idBuilding: this.idBuilding },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refreshTable();
      }
    });
  }

  startEdit(idBuilding: string, idZone: string, nameZone: string, capacity: number,maxCapacity: number) {
    this.idZone= idZone;
    
    console.log(this.idZone);
    debugger;
    const dialogRef = this.dialog.open(EditZoneComponent, {
      data: { idBuilding:idBuilding, idZone: idZone,nameZone: nameZone, capacity: capacity,maxCapacity: maxCapacity}    
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refreshTable();
      }
    });
  }

  deleteItem(idBuilding: string, idZone: string, nameZone: string, capacity: number,maxCapacity: number) {
    this.idZone = idZone;
    const dialogRef = this.dialog.open(DeleteZoneComponent, {
      data: {idBuilding: idBuilding, idZone: idZone,nameZone: nameZone, capacity: capacity, maxCapacity: maxCapacity}
      
    })
    debugger;;

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refreshTable();
      }
    });
  }


}
