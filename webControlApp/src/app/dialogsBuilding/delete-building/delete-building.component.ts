import { Component, OnInit, Inject} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BuildingService } from 'src/app/services/building.service';
import { Building } from 'src/app/model/building.model';

@Component({
  selector: 'app-delete-building',
  templateUrl: './delete-building.component.html',
  styleUrls: ['./delete-building.component.scss']
})
export class DeleteBuildingComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteBuildingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public buildingService: BuildingService) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.buildingService.deleteBuilding(this.data);
    debugger;
  }
}
