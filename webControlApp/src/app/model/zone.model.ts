export class Zone {
    idBuilding: string;
    idZone: string;
    nameZone: string;
    capacity: number;
    maxCapacity: number;

    constructor(
        idBuilding?: string,
    idZone?: string,
        nameZone?: string,
        capacity?: number,
        maxCapacity?: number
        ){ 
            this.idBuilding=idBuilding;
            this.idZone=idZone;
            this.nameZone=nameZone;
            this.capacity=capacity;
            this.maxCapacity=maxCapacity;
        
    }
    
}