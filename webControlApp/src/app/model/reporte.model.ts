import { Time, DatePipe } from '@angular/common';
import { Timestamp } from 'rxjs';

export class Reporte{
   $keyRegistro:string;
   cedula: string;
   edificio: string;
   emailRegistrador: string;
   horaIngreso: DatePipe;
   horaEgreso: DatePipe;
   temperatura: string;
   zona: string
}