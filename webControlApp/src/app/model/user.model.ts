export class User {
    id: string;
    nameUser: string;
    lastNameUser: string;
    identificationUser: string;
    phoneUser: string;
    emailUser: string;
}