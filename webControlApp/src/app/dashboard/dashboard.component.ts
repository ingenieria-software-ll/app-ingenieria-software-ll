import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ReportesService } from '../services/reportes.service';
import { DatePipe } from '@angular/common';
import { element } from 'protractor';
import { MatTableDataSource } from '@angular/material/table';
import jsPDF from 'jspdf'
import 'jspdf-autotable'
import { AppService } from '../services/app.service';
import { MatTableExporterModule } from 'mat-table-exporter';

export class PeriodicElement {
  cedula: string;
  edificio: string;
  emailRegistrador: string;
  horaIngreso: Date;
  horaEgreso: Date;
  temperatura: string;
  zona: string;
}



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  conditionExpression: number = 0;

  displayedColumns: string[] = ['cedula', 'edificio', 'emailRegistrador', 'horaIngreso', 'horaEgreso', 'temperatura', 'zona'];
  dataSource = new MatTableDataSource<PeriodicElement>();
  
  
  constructor(private reportesServices: ReportesService, appService: AppService) { }

  ngOnInit(): void {
  }

  obtenerValorSelect(value: number) {
    this.conditionExpression = value
  }

  getRegistradosPorEdificio(nombreEdificio: string) {

    this.reportesServices.getRegistradosPorEdificio(nombreEdificio).subscribe((data) => {
      let arrayPeriodic: Array<PeriodicElement> = new Array<PeriodicElement>();
      data.forEach((element) => {
        let data = element.payload.doc.data()
        let organizadorTabla: PeriodicElement;
        let horaEgreso = data['horaEgreso'] != undefined? data['horaEgreso'].toDate(): '';
        if (data != undefined) {
          organizadorTabla = {
            cedula: data['cedula'] || ' ',
            edificio: data['edificio'] || ' ',
            emailRegistrador: data['emailRegistrador'] || ' ',
            horaIngreso: data['horaIngreso'].toDate() || ' ',
            horaEgreso: horaEgreso,
            temperatura: data['temperatura'] || ' ',
            zona: data['zona'] || ' '

          }
          arrayPeriodic.push(organizadorTabla);
        }

      })
      this.dataSource.data = arrayPeriodic;
     
    });
  }

  

  getRegistradosPorZona(nombreZona: string) {

    this.reportesServices.getRegistradosPorZona(nombreZona).subscribe((data) => {
      let arrayPeriodic: Array<PeriodicElement> = new Array<PeriodicElement>();
      data.forEach((element) => {
        let data = element.payload.doc.data()
        let organizadorTabla: PeriodicElement;
        let horaEgreso = data['horaEgreso'] != undefined? data['horaEgreso'].toDate(): '';
        if (data != undefined) {
          organizadorTabla = {
            cedula: data['cedula'] || ' ',
            edificio: data['edificio'] || ' ',
            emailRegistrador: data['emailRegistrador'] || ' ',
            horaIngreso: data['horaIngreso'].toDate() || ' ',
            horaEgreso: horaEgreso,
            temperatura: data['temperatura'] || ' ',
            zona: data['zona'] || ' '

          }
          arrayPeriodic.push(organizadorTabla);
        }

      })
      this.dataSource.data = arrayPeriodic;
    });

  }

  print(){
    let doc = new jsPDF(); 
    doc.autoTable({
      head: [['cedula', 'edificio', 'emailRegistrador', 'horaIngreso', 'horaEgreso', 'temperatura', 'zona']],
      body: this.dataSource.data  //returning [["log1", "$100"], ["log2", "$200"]]
    });
    doc.save('Dashboard.pdf')
   
  }

}
