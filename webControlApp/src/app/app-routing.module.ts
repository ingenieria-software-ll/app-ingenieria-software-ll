import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuildingLisComponent } from "./building-lis/building-lis.component";
import { UserListComponent } from "./user-list/user-list.component";
import { LoginComponent } from "./login/login/login.component";
import { ZoneListComponent } from "./zone-list/zone-list.component";
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: '',   redirectTo: '/login', pathMatch: 'full' },
  { path: 'users', component:  UserListComponent},
  { path: 'login', component:  LoginComponent},
  { path: 'building', component: BuildingLisComponent },
  { path: 'building/:id/zone', component: ZoneListComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '**', component:  LoginComponent},
];




@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
