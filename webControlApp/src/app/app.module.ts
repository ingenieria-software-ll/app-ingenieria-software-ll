import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UserService } from './services/user.service';
import { BuildingService } from './services/building.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { UserListComponent } from '../app/user-list/user-list.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AddComponent } from './dialogs/add/add.component';
import { DeleteComponent } from './dialogs/delete/delete.component';
import { EditComponent} from './dialogs/edit/edit.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { LoginComponent } from './login/login/login.component';
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatCardModule} from '@angular/material/card';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { BuildingLisComponent } from './building-lis/building-lis.component';
import { AddBuildingComponent } from './dialogsBuilding/add-building/add-building.component';
import { EditBuildingComponent } from './dialogsBuilding/edit-building/edit-building.component';
import { DeleteBuildingComponent } from './dialogsBuilding/delete-building/delete-building.component';
import { AddZoneComponent } from './dialogsZones/add-zone/add-zone.component';
import { EditZoneComponent } from './dialogsZones/edit-zone/edit-zone.component';
import { DeleteZoneComponent } from './dialogsZones/delete-zone/delete-zone.component';
import { ZoneListComponent } from './zone-list/zone-list.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { NavbarComponent } from './navbar/navbar.component';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppService} from './services/app.service';
import { MatTableExporterModule } from 'mat-table-exporter';





@NgModule({
  
  declarations: [
     AppComponent,
    UserListComponent,
    AddComponent,
    DeleteComponent,
    EditComponent,
    LoginComponent,
    BuildingLisComponent,
    AddBuildingComponent,
    EditBuildingComponent,
    DeleteBuildingComponent,
    AddZoneComponent,
    EditZoneComponent,
    DeleteZoneComponent,
    ZoneListComponent,
    NavbarComponent,
    DashboardComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    FormsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MDBBootstrapModule,
    MatProgressSpinnerModule,
    MatCardModule,
    AngularFireAuthModule,
    LayoutModule,
    MatSidenavModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableExporterModule,
  ],
  exports: [
    MatFormFieldModule,
    MatDialogModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatCardModule,
  ],
  entryComponents: [
    AddComponent,
    EditComponent,
    DeleteComponent,
  ],
  providers: [
    UserService,
    BuildingService,
    AppService
    //AuthService
  ],
  bootstrap: [AppComponent],
})


export class AppModule { }
