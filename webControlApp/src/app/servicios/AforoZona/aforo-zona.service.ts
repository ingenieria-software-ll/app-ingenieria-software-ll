import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class AforoZonaService {



  constructor(private angularfirestore: AngularFirestore) {

  }

  getAforoZona(nombreEdificio: string, nombreZona: string) {
    return this.angularfirestore.collection('Edificios', ref => ref.where('nameBuilding', '==', nombreEdificio)).get().subscribe((data) => {
      return this.angularfirestore.collection('Edificios').doc(data.docs[0].id).collection('zona', ref => ref.where('nameZone', '==', nombreZona)).get().subscribe((data2) => {
        return data2.docs[0].data();
      })
    })

  }
}
