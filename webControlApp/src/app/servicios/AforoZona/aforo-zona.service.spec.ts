import { TestBed } from '@angular/core/testing';

import { AforoZonaService } from './aforo-zona.service';

describe('AforoZonaService', () => {
  let service: AforoZonaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AforoZonaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
