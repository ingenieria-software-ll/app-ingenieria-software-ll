import { BuildingService } from 'src/app/services/building.service';
import { Building } from 'src/app/model/building.model';
import { element } from 'protractor';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { fromEvent} from 'rxjs';
import { AddBuildingComponent } from '../dialogsBuilding/add-building/add-building.component'
import { DeleteBuildingComponent } from '../dialogsBuilding/delete-building/delete-building.component'
import { EditBuildingComponent } from '../dialogsBuilding/edit-building/edit-building.component'
import { Router } from '@angular/router'

@Component({
  selector: 'app-building-lis',
  templateUrl: './building-lis.component.html',
  styleUrls: ['./building-lis.component.css']
})
export class BuildingLisComponent implements OnInit {

  displayedColumns = ['nameBuilding','actions'];
  buildings: Building[] = [];
  buildingsFiltered: Building[] = [];
  idBuilding: string;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;


  constructor(public dialog: MatDialog,
    public buildingService: BuildingService,
    private router: Router) { }

  ngOnInit(): void {
    
    this.loadData();
    fromEvent(this.filter.nativeElement, 'keyup')
      .subscribe(() => {
        if (this.filter.nativeElement.value === '') {
          this.buildingsFiltered = this.buildings;
        } else {
          this.buildingsFiltered = this.buildings.filter(building => building.nameBuilding !== undefined && building.nameBuilding !== null && building.nameBuilding.indexOf(this.filter.nativeElement.value) !== -1);
        }
      })
  }

  refresh(): void {
    this.loadData();
  }



  public async loadData() {
    this.buildingService.getBuilding().subscribe(data => {
      this.buildings = data.map(e => {
        let building = e.payload.doc.data();
        return {
          idBuilding: e.payload.doc.id,
          nameBuilding: building['nameBuilding']
        } as Building
      });;;
      this.buildingsFiltered = this.buildings;
    });
  }

  
  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  addNew() {
    const dialogRef = this.dialog.open(AddBuildingComponent, {
      data: { building: Building }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refreshTable();
      }
    });
  }

  startEdit(idBuilding: string, nameBuilding: string) {
    this.idBuilding= idBuilding;
    
    console.log(this.idBuilding);
    debugger;
    const dialogRef = this.dialog.open(EditBuildingComponent, {
      data: { idBuilding: idBuilding, nameBuilding: nameBuilding}    
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refreshTable();
      }
    });
  }

  deleteItem(idBuilding: string, nameBuilding: string) {
    this.idBuilding = idBuilding;
    const dialogRef = this.dialog.open(DeleteBuildingComponent, {
      data: {idBuilding: idBuilding, nameBuilding: nameBuilding}
      
    })
    debugger;;

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refreshTable();
      }
    });
  }

  goToZones(idBuilding: string){
    this.router.navigate(["building/"+idBuilding+"/zone"]);
  }

}
