import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildingLisComponent } from './building-lis.component';

describe('BuildingLisComponent', () => {
  let component: BuildingLisComponent;
  let fixture: ComponentFixture<BuildingLisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildingLisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildingLisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
