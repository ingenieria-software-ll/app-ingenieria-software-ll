// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCsqCOPGVbgOLJyu5yjEOcuP-jk9NmhewQ",
    authDomain: "controlcovid-89cea.firebaseapp.com",
    databaseURL: "https://controlcovid-89cea.firebaseio.com",
    projectId: "controlcovid-89cea",
    storageBucket: "controlcovid-89cea.appspot.com",
    messagingSenderId: "32860941680",
    appId: "1:32860941680:web:9dacd4f581b3e724fa33b5",
    measurementId: "G-GH46HWT6HD"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
