import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'listarusuarios.dart';

class FormUser extends StatefulWidget {
  FormUser({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return FormUserState();
  }
}

class FormUserState extends State<FormUser> {
  final _formKey = new GlobalKey<FormState>();

  String _name;
  String _lastName;
  String _identification;
  String _email;
  String _phone;
  String _homeAddress;
  final databaseReference = Firestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [_showForm()],
      ),
    );
  }

  Widget _showForm() {
    return new Container(
        padding: EdgeInsets.all(16.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              _title(),
              _buildName(),
              _buildLastName(),
              _buildIdentification(),
              _buildEmail(),
              _buildPhone(),
              _buildHomeAddress(),
              _showPrimaryButton()
            ],
          ),
        ));
  }

  Widget _title() {
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
      BackButton(
        color: Colors.black,
      ),
      Text(
        'Registrar visitante',
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
    ]);
  }

  Widget _buildName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Nombre',
            icon: new Icon(
              Icons.account_circle,
              color: Colors.grey,
            )),
        validator: (String value) {
          if (value.isEmpty) {
            return 'Este campo es obligatorio';
          }
        },
        onSaved: (value) => _name = value.trim(),
      ),
    );
  }

  Widget _buildLastName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Apellido',
            icon: new Icon(
              Icons.account_circle,
              color: Colors.grey,
            )),
        validator: (String value) {
          if (value.isEmpty) {
            return 'Este campo es obligatorio';
          }
        },
        onSaved: (value) => _lastName = value.trim(),
      ),
    );
  }

  Widget _buildIdentification() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Identificación',
            icon: new Icon(
              Icons.credit_card,
              color: Colors.grey,
            )),
        validator: (String value) {
          if (value.isEmpty) {
            return 'Este campo es obligatorio';
          }
        },
        onSaved: (value) => _identification = value.trim(),
      ),
    );
  }

  Widget _buildEmail() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Correo electrónico',
            icon: new Icon(
              Icons.email,
              color: Colors.grey,
            )),
        onSaved: (value) => _email = value.trim(),
      ),
    );
  }

  Widget _buildPhone() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Telefono',
            icon: new Icon(
              Icons.phone,
              color: Colors.grey,
            )),
        validator: (String value) {
          if (value.isEmpty) {
            return 'Este campo es obligatorio';
          }
        },
        onSaved: (value) => _phone = value.trim(),
      ),
    );
  }

  Widget _buildHomeAddress() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Direccion',
            icon: new Icon(
              Icons.home,
              color: Colors.grey,
            )),
        onSaved: (value) => _homeAddress = value.trim(),
      ),
    );
  }

  Widget _showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: Colors.blue,
            child: new Text('Guardar',
                style: new TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: _validateAndSubmit,
          ),
        ));
  }

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit(){
    if(validateAndSave()){
      final personalData = {
        'nombre': _name,
        'apellido': _lastName,
        'identificacion': _identification,
        'correo' : _email,
        'telefono' : _phone,
        'direccion' : _homeAddress,
      };
      createData(personalData);
      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListaUsuarios()),
                        );
    }
  }
  

  void createData (Map<String,dynamic> data) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String uid = prefs.getString('uid');
    await databaseReference.collection('visitantes').document(uid).setData(data);
  }

}
