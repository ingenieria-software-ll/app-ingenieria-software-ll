import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:control_covid/Models/registroEntrada.model.dart';
import 'package:control_covid/Services/registrarEntrada.service.dart';
import 'package:control_covid/Views/login.dart';
import 'package:control_covid/Views/searchUserByCc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import 'listarusuarios.dart';

class RegistrarIngreso extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegistrarIngresoState();
  }
}

class RegistrarIngresoState extends State<RegistrarIngreso> {
  String cedula;
  String temperatura;
  String zonaDestino;
  String dropdawnvalue = "Cedula";
  String dropdawnvalue2 = " ";

  List<String> listadropdawnEdificio = List();
  List<String> listaZonas = List();

  RegistrarEntrada registrarEntrada = new RegistrarEntrada();

  List<DocumentSnapshot> listaInformacion = List();

  bool primeraSangre = false;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildCedula() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Cedula'),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Este campo es obligatorio';
        }
      },
      onChanged: (String value) {
        cedula = value;
      },
    );
  }

  Widget _buildTemperatura() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Temperatura'),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Este campo es obligatorio';
        }
      },
      onChanged: (String value) {
        temperatura = value;
      },
    );
  }

  encontrarEdificio() {
    this.registrarEntrada.obtenerTodosEdificios().listen((value) {
      List<DocumentSnapshot> lista = value.documents;
      List<String> listaString = List();
      lista.forEach((element) {
        listaString.add(element.data["nameBuilding"]);
      });
      setState(() {
        listadropdawnEdificio = listaString;
        listaInformacion = lista;
        if (!primeraSangre) {
          dropdawnvalue = lista[0].data["nameBuilding"];
          primeraSangre = true;
        }
      });
    });
  }

  Widget _buildZonaDestino() {
    return DropdownButton<String>(
      value: dropdawnvalue2,
      icon: Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(color: Colors.red),
      underline: Container(
        height: 2,
        color: Colors.red,
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdawnvalue2 = newValue;
        });
      },
      items: listaZonas.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value != null ? value : ''),
        );
      }).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    encontrarEdificio();
    return Scaffold(
      appBar: AppBar(
        title: Text("Regitrar entrada "),
        actions: [
          IconButton(
              icon: Icon(Icons.archive),
              onPressed: () {
                _auth.signOut();
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Login()),
                );
              })
        ],
      ),
      body: Container(
        margin: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildCedula(),
                _buildTemperatura(),
                _buildSeleccionarEdificio(),
                _buildZonaDestino(),
                SizedBox(height: 30),
                RaisedButton(
                  child: Text(
                    "Registrar Ingreso",
                    style: TextStyle(color: Colors.blue, fontSize: 16),
                  ),
                  onPressed: () async {
                    RegistroEntrada registroEntrada = new RegistroEntrada();
                    registroEntrada.identificationUser = cedula;
                    registroEntrada.edificio = dropdawnvalue;
                    registroEntrada.temperatura = temperatura;
                    registroEntrada.zonaDestino = dropdawnvalue2;

                    var existeRegistro = await registrarEntrada
                        .verificarIngresado(registroEntrada.identificationUser);

                    if (!existeRegistro) {
                      Toast.show(
                          "El usuario no ha registrado salida. ", context,
                          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                      return;
                    } else {
                      listaZonas.clear();
                      listaInformacion.clear();
                      listadropdawnEdificio.clear();
                    }

                    this
                        .registrarEntrada
                        .registrarEntrada(registroEntrada, context)
                        .then((value) => {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ListaUsuarios()),
                              ),
                            });
                  },
                ),
                RaisedButton(
                  child: Text(
                    "Visitantes",
                    style: TextStyle(color: Colors.blue, fontSize: 16),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ListaUsuarios()),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSeleccionarEdificio() {
    return DropdownButton<String>(
      value: dropdawnvalue,
      icon: Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(color: Colors.red),
      underline: Container(
        height: 2,
        color: Colors.red,
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdawnvalue = newValue;
          obtenerZonas();
        });
      },
      items:
          listadropdawnEdificio.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value != null ? value : ''),
        );
      }).toList(),
    );
  }

  void obtenerZonas() {
    setState(() {
      listaZonas.clear();
      // dropdawnvalue2 = "";
    });

    this
        .listaInformacion
        .where((DocumentSnapshot element) =>
            (element.data["nameBuilding"] == dropdawnvalue))
        .toList()
        .forEach((element2) {
      this
          .registrarEntrada
          .obtenerZonasPorEdificio(element2.documentID)
          .listen((event) {
        setState(() {
          dropdawnvalue2 = event.documents[0].data["nameZone"];
        });
        event.documents.forEach((element3) {
          setState(() {
            listaZonas.add(element3.data["nameZone"]);
          });
        });
      });
    });

    // .map((DocumentSnapshot e) =>
    //     }));
  }
}
