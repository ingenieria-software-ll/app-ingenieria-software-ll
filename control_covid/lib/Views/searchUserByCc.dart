import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:control_covid/Services/consultas.dart';
import 'package:control_covid/Views/registrarIngreso.view.dart';
import 'package:flutter/material.dart';
import 'formUser.dart';

class SearchUserByCc extends StatefulWidget {
  SearchUserByCc({Key key}) : super(key: key);

  @override
  _SearchUserByCcState createState() => _SearchUserByCcState();
}

class _SearchUserByCcState extends State<SearchUserByCc> {
  Consultas consultar = new Consultas();
  String dropdawnvalue = 'Cedula';
  List<DocumentSnapshot> listausuarios = List();
  List<DocumentSnapshot> listausuariosfiltrada = List();
  String inputvalue = "";
  @override
  Widget build(BuildContext context) {
    buscarUsuarios();
    return Container(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Buscar Usuario por documento"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RegistrarIngreso()),
                );
              },
            ),
          ],
        ),
        body: Container(
            child: Column(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 40),
                child: DropdownButton<String>(
                  value: dropdawnvalue,
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 24,
                  elevation: 16,
                  style: TextStyle(color: Colors.red),
                  underline: Container(
                    height: 2,
                    color: Colors.red,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      dropdawnvalue = newValue;
                    });
                    filtrarLista();
                  },
                  items: <String>['Cedula', 'Documento interno']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                )),
            Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 40),
                child: Container(
                    child: Expanded(
                        child: SizedBox(
                  child: TextField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.search),
                      border: InputBorder.none,
                    ),
                    onChanged: (value) => {
                      setState(() {
                        inputvalue = value;
                      }),
                      filtrarLista(),
                    },
                  ),
                )))),
            listausuariosfiltrada.isNotEmpty
                ? Expanded(
                    child: SizedBox(
                        child: ListView.builder(
                    itemCount: listausuariosfiltrada.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        child: Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.all(5),
                              child: CircleAvatar(
                                child: Text((listausuariosfiltrada[index]
                                        ['Nombre'])
                                    .split('')[0]),
                              ),
                            ),
                            Center(
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width /
                                        4.5),
                                child: Column(
                                  children: [
                                    Text(
                                      listausuariosfiltrada[index]['Nombre'] +
                                          ' ' +
                                          listausuariosfiltrada[index]
                                              ['Apellido'],
                                      textAlign: TextAlign.center,
                                    ),
                                    Text('CC: ' +
                                        listausuariosfiltrada[index]['Cedula']),
                                    listausuariosfiltrada[index]
                                                ['Documento interno'] !=
                                            null
                                        ? Text('DI: ' +
                                            listausuariosfiltrada[index]
                                                ['Documento interno'])
                                        : Container()
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    },
                  )))
                : CircularProgressIndicator()
          ],
        )),
      ),
    );
  }

  bool firsttime = true;
  void buscarUsuarios() {
    this.consultar.traerUsuarios().listen((event) {
      setState(() {
        if (firsttime) {
          listausuariosfiltrada = event.documents;
          firsttime = false;
        }
        listausuarios = event.documents;
      });
    });
  }

  filtrarLista() {
    String value = this.inputvalue;
    setState(() {
      listausuariosfiltrada = listausuarios.where((element) {
        if (dropdawnvalue == "Cedula") {
          String stringelemento = element.data["Cedula"];
          return stringelemento.contains(value);
        }
        if (dropdawnvalue == "Documento interno") {
          if (element.data["Documento interno"] == null) {
            return false;
          } else {
            String stringelemento = element.data["Documento interno"];
            return stringelemento.contains(value);
          }
        }
        return false;
      }).toList();
    });
  }
}
