import 'package:control_covid/Services/autService.dart';
import 'package:control_covid/Views/registrarIngreso.view.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<Login> {
  String _emailSession = "";
  String _password = "";
  final FirebaseAuth _auth = FirebaseAuth.instance;
  bool cargando = true;

  Autentication autenticar = new Autentication();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  LoginState() {
    obtenerUsuarioActual();
  }

  Widget _buildImagen() {
    return Image.network(
        "https://es.seaicons.com/wp-content/uploads/2015/08/red-user-icon.png");
  }

  Widget _buildEmailSession() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Correo electrónico',
        prefixIcon: Icon(Icons.email),
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Este campo es obligatorio';
        }
      },
      onChanged: (String value) {
        _emailSession = value;
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Contraseña',
        prefixIcon: Icon(Icons.lock_outline),
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Este campo es obligatorio';
        }
      },
      onChanged: (String value) {
        _password = value;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if (cargando) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Inicio de sesión"),
      ),
      body: Container(
        margin: EdgeInsets.all(30),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildImagen(),
                _buildEmailSession(),
                _buildPassword(),
                SizedBox(height: 30),
                RaisedButton(
                  child: Text(
                    "Iniciar sesión",
                    style: TextStyle(color: Colors.red[900], fontSize: 16),
                  ),
                  onPressed: () {
                    try {
                      autenticar.login(_emailSession, _password).then((value) {
                        Navigator.of(context).pop();
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegistrarIngreso()),
                        );
                      });
                    } catch (error) {
                      String _error;
                      print('Falle al registrar ${error.code}');
                      switch (error.code) {
                        case 'ERROR_EMAIL_ALREADY_IN_USE':
                          _error = 'El correo ya esta en uso.';
                          break;
                        case 'ERROR_WEAK_PASSWORD':
                          _error =
                              'Por favor ingrese una contraseña más larga.';
                          break;
                        case 'ERROR_INVALID_EMAIL':
                          _error = 'Ingresaste un correo no valido.';
                          break;
                        default:
                          _error = 'Por favor revisa tu correo y contraseña.';
                          break;
                      }
                      Toast.show("$_error", context,
                          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> obtenerUsuarioActual() async {
    FirebaseUser currentUser = await _auth.currentUser();
    if (currentUser != null) {
      Navigator.of(context).pop();
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => RegistrarIngreso()),
      );
    } else {
      setState(() => cargando = false);
    }
  }
}
