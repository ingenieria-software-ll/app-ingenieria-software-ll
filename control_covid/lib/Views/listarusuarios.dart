import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:control_covid/Services/consultas.dart';
import 'package:control_covid/Services/registrarSalida.service.dart';
import 'package:control_covid/Views/registrarIngreso.view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class ListaUsuarios extends StatefulWidget {
  ListaUsuarios({Key key}) : super(key: key);

  @override
  _ListaUsuariosState createState() => _ListaUsuariosState();
}

class _ListaUsuariosState extends State<ListaUsuarios> {
  Consultas consultar = new Consultas();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text("Lista de Visitantes"),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.add),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RegistrarIngreso()),
                );
              }),
        ],
        leading: new Container(),
      ),
      body: StreamBuilder(
        stream: consultar.traerRegistrados(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else {
            var documentos = snapshot.data.documents;
            return ListView.builder(
              itemCount: documentos.length,
              itemBuilder: (context, index) {
                bool bandera = documentos[index]['horaEgreso'] == null;
                return ListTile(
                  title: Text(documentos[index]['cedula']),
                  subtitle: Text(documentos[index]['edificio'] +
                      " " +
                      documentos[index]['zona']),
                  trailing: IconButton(
                      color: bandera ? null : Colors.red,
                      icon: Icon(Icons.exit_to_app),
                      onPressed: () => bandera
                          ? registrarSalida(
                              snapshot.data.documents[index].documentID,
                              context)
                          : Toast.show("Ya se registró la salida. ", context,
                              duration: Toast.LENGTH_LONG,
                              gravity: Toast.CENTER)),
                );
              },
            );
          }
        },
      ),
    );
  }
}
