import 'package:firebase_auth/firebase_auth.dart';

class Autentication {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future login(String correo, String password) {
    return _auth.signInWithEmailAndPassword(email: correo, password: password);
  }

  Future logout() {
    return _auth.signOut();
  }
}
