import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:control_covid/Models/registroEntrada.model.dart';
import 'package:control_covid/Views/listarusuarios.dart';
import 'package:control_covid/Views/searchUserByCc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class RegistrarEntrada {
  final Firestore _db = Firestore.instance;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Stream<QuerySnapshot> obtenerTodosEdificios() {
    return _db.collection('Edificios').snapshots();
  }

  Stream<QuerySnapshot> obtenerZonasPorEdificio(String edificio) {
    return _db
        .collection('Edificios')
        .document(edificio)
        .collection("zona")
        .snapshots();
  }

  Future<bool> verificarIngresado(String cedula) async {
    var documentos = (await _db
            .collection('RegistrodeAcceso')
            .where('cedula', isEqualTo: cedula)
            .where('horaEgreso', isNull: true)
            .getDocuments())
        .documents;

    return documentos.isEmpty;
  }

  Future registrarEntrada(RegistroEntrada registroEntrada, context) async {
    FirebaseUser currentUser = await _auth.currentUser();
    return _db
        .collection('visitantes')
        .where('identificationUser',
            isEqualTo: registroEntrada.identificationUser)
        .getDocuments()
        .then((value) => {
              value.documents.isEmpty
                  ? Toast.show("El usuario no ha sido encontrado. ", context,
                      duration: Toast.LENGTH_LONG, gravity: Toast.CENTER)
                  : _db
                      .collection('Edificios')
                      .where('nameBuilding',
                          isEqualTo: registroEntrada.edificio)
                      .getDocuments()
                      .then((value2) => {
                            value2.documents.isEmpty
                                ? Toast.show(
                                    "No se encontró el edificio. ", context,
                                    duration: Toast.LENGTH_LONG,
                                    gravity: Toast.CENTER)
                                : _db
                                    .collection('Edificios')
                                    .document(value2.documents[0].documentID)
                                    .collection('zona')
                                    .where('nameZone',
                                        isEqualTo: registroEntrada.zonaDestino)
                                    .getDocuments()
                                    .then((value3) => {
                                          value3.documents.isEmpty
                                              ? Toast.show(
                                                  "No se encontró la zona. ", context,
                                                  duration: Toast.LENGTH_LONG,
                                                  gravity: Toast.CENTER)
                                              : value3.documents[0]
                                                          .data['capacity'] >=
                                                      value3.documents[0]
                                                          .data['maxCapacity']
                                                  ? Toast.show(
                                                      "No hay más capacidad en la zona. ",
                                                      context,
                                                      duration:
                                                          Toast.LENGTH_LONG,
                                                      gravity: Toast.CENTER)
                                                  : _db
                                                      .collection('Edificios')
                                                      .document(value2
                                                          .documents[0]
                                                          .documentID)
                                                      .collection('zona')
                                                      .document(value3
                                                          .documents[0]
                                                          .documentID)
                                                      .updateData({'capacity': value3.documents[0].data['capacity'] + 1}).then((value) => {
                                                            _db
                                                                .collection(
                                                                    'RegistrodeAcceso')
                                                                .add({
                                                              'cedula':
                                                                  registroEntrada
                                                                      .identificationUser,
                                                              'edificio':
                                                                  registroEntrada
                                                                      .edificio,
                                                              'temperatura':
                                                                  registroEntrada
                                                                      .temperatura,
                                                              'zona':
                                                                  registroEntrada
                                                                      .zonaDestino,
                                                              'horaIngreso':
                                                                  DateTime
                                                                      .now(),
                                                              'horaEgreso':
                                                                  null,
                                                              'edificioID': value2
                                                                  .documents[0]
                                                                  .documentID,
                                                              'zonaID': value3
                                                                  .documents[0]
                                                                  .documentID,
                                                              'emailRegistrador':
                                                                  currentUser
                                                                      .email
                                                            }).then((value) => {
                                                                      Toast.show(
                                                                          "Se registró la entrada. ",
                                                                          context,
                                                                          duration: Toast
                                                                              .LENGTH_LONG,
                                                                          gravity:
                                                                              Toast.CENTER),
                                                                    })
                                                          })
                                        })
                          })
            });
  }
}
