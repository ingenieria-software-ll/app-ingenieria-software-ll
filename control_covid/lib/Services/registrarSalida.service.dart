import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:toast/toast.dart';

Firestore _db = Firestore.instance;

Future<void> registrarSalida(String id, context) async {
  var registroSalida =
      (await _db.collection('RegistrodeAcceso').document(id).get()).data;

  await _db
      .collection('RegistrodeAcceso')
      .document(id)
      .setData({'horaEgreso': DateTime.now()}, merge: true);
  var cambiarAforo = (await _db
          .collection('Edificios')
          .document(registroSalida['edificioID'])
          .collection('zona')
          .document(registroSalida['zonaID'])
          .get())
      .data;
  await _db
      .collection('Edificios')
      .document(registroSalida['edificioID'])
      .collection('zona')
      .document(registroSalida['zonaID'])
      .setData({'capacity': cambiarAforo['capacity'] - 1}, merge: true);
  Toast.show("Se registró la salida. ", context,
      duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
}
