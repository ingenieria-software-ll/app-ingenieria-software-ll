import 'package:cloud_firestore/cloud_firestore.dart';

class Consultas {
  final Firestore _db = Firestore.instance;

  Stream<QuerySnapshot> traerUsuarios() {
    return _db.collection('visitantes').snapshots();
  }

  Stream<QuerySnapshot> traerRegistrados() {
    return _db.collection('RegistrodeAcceso').snapshots();
  }
}
