import 'package:flutter/material.dart';
//import 'package:control_covid/formUser.dart';
import 'package:control_covid/Views/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Control Covid',
      theme: ThemeData(
        primaryColor: Colors.red[900],
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Login(),
    );
  }
}
